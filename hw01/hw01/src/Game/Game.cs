﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hw01
{
    public class Game : IGame
    {

        public State State { get; set; }

        public void Init()
        {
            State = Utils.Init();
        }

        public void Run()
        {

            while (!State.Exit)
            {
                var command = Utils.ParseCommand(ConsoleHandler.ConsoleProvider.Read());
                switch (command.Type)
                {
                    case CommandType.Exit:
                        State.Exit = true;
                        break;
                    case CommandType.Start when !State.AlreadyPicked:
                        State.Pick(command);
                        break;
                    case CommandType.Reorder when State.AlreadyPicked:
                        State.Reorder(command.Params);
                        break;
                    case CommandType.Rip when State.AlreadyPicked:
                        State.Rip();
                        break;
                    case CommandType.Inspect:
                        State.Inspect();
                        break;
                    case CommandType.Info when State.AlreadyPicked:
                        State.Info();
                        break;
                    case CommandType.Fight when State.AlreadyPicked:
                        State.Fight();
                        break;
                    case CommandType.Fight:
                    case CommandType.Info:
                    case CommandType.Reorder:
                    case CommandType.Start:
                    case CommandType.Rip:
                        Utils.BadStateCommand(command);
                        break;
                    default:
                        Utils.BadCommand(command);
                        
                        break;
                }
            }
            Utils.Finish();
        }
    }
}
