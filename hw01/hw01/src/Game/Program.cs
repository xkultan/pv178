﻿using System;

namespace Hw01.src
{
    class Program
    {
        public static int Main()
        {
            try
            {
                IGame game = new Game();
                game.Init();
                game.Run();
            } catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1;
            }
            return 0;
        }
    }
}