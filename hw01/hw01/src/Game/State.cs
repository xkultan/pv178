﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hw01
{
    public class State
    {
        private List<Hero> _generatedHeroes = new List<Hero>();
        private List<List<Hero>> _caves = new List<List<Hero>>();
        public Player Player { get; set; }
        public List<Hero> Heroes { get { return Player.Heroes; } set { Player.Heroes = value; } }
        public List<Hero> GeneratedHeroes { get { return _generatedHeroes; } }
        public List<List<Hero>> Caves { get { return _caves; } }
        public bool AlreadyPicked { get { return Player.Heroes.Count != 0; } }
        public bool Exit { get; set; }


        public State()
        {
            Init();
        }

        public void Init()
        {
            Player = new Player();
            GenerateHeroes();
            GenerateCaves();
        }

        public void GenerateCaves()
        {
            for (int caveIndex = 7; caveIndex > 0; caveIndex--)
            {
                List<Hero> cave = new List<Hero>();
                for (int i = 0; i < 3; i++)
                {
                    int health, damage, speed;
                    Utils.GenerateStats(1 + (0.2 * caveIndex), out health, out damage, out speed);
                    string name = Utils.NameGenerator.GenerateName();
                    cave.Add(new Hero(name, health, damage, speed, -1, Utils.GenerateHeroType()));
                }
                Caves.Add(cave);
            }
        }

        private void GenerateHeroes()
        {
            for (int i = 0; i < Const.NumberOfGeneratedHeroes; i++)
            {
                int health, damage, speed;
                Utils.GenerateStats(1, out health, out damage, out speed);
                string name = Utils.NameGenerator.GenerateName();
            
                GeneratedHeroes.Add(new Hero(name, health, damage, speed, 0, Utils.GenerateHeroType()));
            }
        }

        public void PrintGeneratedHeroes(bool withIndexes, bool onlyName)
        {
            Utils.PrintHeroes(GeneratedHeroes, withIndexes, onlyName);
        }

        public void PrintPickedHeroes(bool withIndexes, bool onlyName)
        {
            Utils.PrintHeroes(Heroes, withIndexes, onlyName);
        }

        public void Pick(Command command)
        {
            if (command.Params.Length != 3)
            {
                ConsoleHandler.ConsoleProvider.WriteLine(Const.BadInputParams, command.Params.Length, 3);
                return;
            }
            int[] args = Array.ConvertAll(command.Params, int.Parse).ToArray();
            if (args.Any(x => x > GeneratedHeroes.Count || x < 1))
            {
                ConsoleHandler.ConsoleProvider.WriteLine(Const.BadHeroIndex, args.First(x => x > GeneratedHeroes.Count || x < 1));
                return;
            }
            foreach (int i in args)
            {
                Player.Heroes.Add(GeneratedHeroes[i - 1]);
            }
            ConsoleHandler.ConsoleProvider.Write("You have choosen ");
            Utils.PrintHeroName(Heroes[0], false);
            ConsoleHandler.ConsoleProvider.Write(", ");
            Utils.PrintHeroName(Heroes[1], false);
            ConsoleHandler.ConsoleProvider.Write(" and ");
            Utils.PrintHeroName(Heroes[2], false);
            ConsoleHandler.ConsoleProvider.WriteLine("");
            ConsoleHandler.ConsoleProvider.WriteLine(Const.Commands);
        }

        public bool Reorder(string[] str_args)
        {
            int[] args;
            PrintPickedHeroes(true, true);
            if (str_args.Length == 0)
            {
                args = Utils.ReadReorderArgs();
            }
            else if (str_args.Any(x => x[0] < '0' || x[0] > '3'))
            {
                ConsoleHandler.ConsoleProvider.WriteLine(Const.BadHeroIndex, str_args.First(x => x[0] < '0' || x[0] > '3'));
                args = Utils.ReadReorderArgs();
            }
            else
            {
                args = Array.ConvertAll(str_args, int.Parse).ToArray();
            }
            

            List<Hero> newHeroes = new List<Hero>();
            foreach (int i in args)
            {
                newHeroes.Add(Heroes[i - 1]);
            }
            Heroes = newHeroes;
            ConsoleHandler.ConsoleProvider.WriteLine(Const.ReorderDone);
            return true;
        }

        public void Rip()
        {
            ConsoleHandler.ConsoleProvider.WriteLine(Const.Rip);
            Exit = true;
        }

        public void Inspect()
        {
            if (!AlreadyPicked)
            {
                ConsoleHandler.ConsoleProvider.WriteLine(Const.InspectNotStarted);
                return;
            }
            ConsoleHandler.ConsoleProvider.WriteLine(Const.Inspect);
            Utils.PrintHeroes(Caves.Last(), false, false);
        }

        public void Info()
        {
            Utils.PrintInfo(Player);
        }

        public bool Fight()
        {
            int round = 1;
            Player.Fight++;
            while (true)
            {
                if (Heroes.All(x => x.Health <= 0))
                {
                    Utils.CaveEnd(Player, false);
                    
                    return false;
                }
                if (Caves.Last().All(x => x.Health <= 0))
                {
                    Utils.CaveEnd(Player, true);
                    Caves.Remove(Caves.Last());
                    if (Player.DiamondPieces == 7)
                    {
                        ConsoleHandler.ConsoleProvider.WriteLine(Const.GamePassed, Player.Fight);
                        Exit = true;
                    }
                    return true;
                }
                Hero hero = Heroes.First(x => x.Health > 0);
                Hero protector = Caves.Last().First(x => x.Health > 0);
                Utils.PrintRound(round, hero, protector);
                hero.AttackHero(protector);
                round++;
            }
        }
    }
}
