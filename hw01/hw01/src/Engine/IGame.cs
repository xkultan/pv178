﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hw01
{
    public interface IGame
    {
        void Init();

        void Run();
    }
}
