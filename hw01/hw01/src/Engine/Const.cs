﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hw01
{
    public static class Const
    {
        public const int NumberOfGeneratedHeroes = 6;

        public const string StartText = "Welcome to The Quest for Carrefour! Please, choose three adventurers:";
        public const string Delimeter = "========================";
        public const string Commands = "Your commands are: inspect, fight, info, reorder, rip";
        public const string Prompt = "Player:> ";
        public const string BadInputParams = "Bad number of params: {0}, {1} are required";
        public const string BadHeroIndex = "Hero with number {0} does not exist";
        public const string ReorderDone = "The order of your adventurers has been updated.";
        public const string Rip = "You have lost the game :(";
        public const string InspectNotStarted = "Game hasn't started yet";
        public const string Inspect = "The next diamond piece is guarded by these enemies:";
        public const string Info = "Diamond pieces: {0}\nYour adventurers:";
        public const string BadStateCommand = "{0} is not supported in this state";
        public const string BadCommand = "{0} is not suported command";
        public const string Round = "Round {0}: ";
        public const string DamageDealt = " dealt {0} damage to ";
        public const string CurrentHealth = " has currently {0} HP.";
        public const string Defeated = " is defeated!";
        public const string CavePassed = "You have won the match! Adventurers gain {0} XP! You acquired the diamond piece!";
        public const string CaveFailed = "You have lost the match... Adventurers gain only {0} XP... You did not acquire the diamond piece...";
        public const string LevelUp = "Your adventurers leveled up!";
        public const string GamePassed = "You have succesfully passed the game, you fight {0} times!";
        public const string Finish = "Press any key to close this window...";

        public static readonly Dictionary<HeroTypeEnum, List<HeroTypeEnum>> EficiencyAttack = new Dictionary<HeroTypeEnum, List<HeroTypeEnum>>()
        {
            {HeroTypeEnum.red, new List<HeroTypeEnum>() { HeroTypeEnum.green} },
            {HeroTypeEnum.blue, new List<HeroTypeEnum>() {HeroTypeEnum.red} },
            {HeroTypeEnum.green, new List<HeroTypeEnum>() {HeroTypeEnum.blue} }
        };

        public static List<string> HeroName = new List<string>() {"Rayna", "Dillian", "Oswyn",
            "Tamina", "Llana", "Laerdya", "Celethir", "Erunonidan", "Mylaela", "Aranadan",
            "Thundruil", "Halrod", "Vallath", "Laeros", "Calothosk", "Deagh", "Sakata",
            "Calanon", "Mateo", "Maldor", "Wiebe", "Iiri", "Kerbasi", "Erubadhron",
            "Haka", "Erynion", "Alagos", "Faelon", "Talfagoron", "Stodgy"};
    }
}
