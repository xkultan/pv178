﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hw01
{
    public enum HeroTypeEnum
    {
        green, blue, red
    }

    public enum CommandType
    {
        Start, Inspect, Fight, Info, Reorder, Rip, Exit=-1
    }
}
