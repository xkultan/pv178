﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hw01
{
    public class Command
    {
        public CommandType? Type { get; }
        public string[] Params { get; }
        public string FullCommand { get; }

        public Command(CommandType type, string input)
        {
            FullCommand = input;
            Type = type;
            Params = input.Split().Skip(1).ToArray();
        }

        public Command(string input)
        {
            input = input.Trim();
            FullCommand = input;
            string[] args = input.Split();
            try
            {
                Type = (CommandType)Enum.Parse(typeof(CommandType), args[0], true);
            }
            catch
            {
                Type = null;
            }
            Params = args.Skip(1).ToArray();
        }
    }
}
