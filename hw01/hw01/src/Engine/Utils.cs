﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hw01
{


    public static class Utils
    {
        private static Random random = new Random();
        public static NameGenerator NameGenerator = new NameGenerator();

        public static State Init()
        {
            ConsoleHandler.ConsoleProvider.WriteLine(Const.StartText);
            ConsoleHandler.ConsoleProvider.WriteLine(Const.Delimeter);
            State state = new State();
            state.PrintGeneratedHeroes(true, false);
            ConsoleHandler.ConsoleProvider.WriteLine(Const.Delimeter);
            return state;
        }

        public static void GenerateStats(double scale, out int health, out int damage, out int speed)
        {
            health = (int)((5 + random.Next(5)) * scale);
            damage = (int)((4 + random.Next(4)) * scale);
            speed = (int)((4 + random.Next(4)) * scale);
        }

        public static HeroTypeEnum GenerateHeroType()
        {
            var enumList = Enum.GetValues(typeof(HeroTypeEnum));
            return (HeroTypeEnum)enumList.GetValue(random.Next(enumList.Length));
        }

        public static Command ParseCommand(string input)
        {
            return new Command(input);
        }

        public static int[] ReadReorderArgs()
        {
            int[] args;
            while (true)
            {
                string command = ConsoleHandler.ConsoleProvider.Read();
                try
                {
                    args = Array.ConvertAll(command.Split(), int.Parse);
                }
                catch (Exception ex)
                {
                    ConsoleHandler.ConsoleProvider.WriteLine("Bad input for reorder");
                    continue;
                }
                if (args.Any(x => x < 1 || x > 3))
                {
                    Console.WriteLine(Const.BadHeroIndex, args.First(x => x > 3 || x < 1));
                }
                else if (args.Length < 4 && args.Length > 0)
                {
                    return args;
                }
                else
                {
                    ConsoleHandler.ConsoleProvider.WriteLine(Const.BadInputParams, args.Length, 3);
                }
            }
        }

        public static void PrintHeroes(List<Hero> heroes, bool withIndex, bool moreInfo)
        {
            for (int i = 0; i < heroes.Count; i++)
            {
                ConsoleColor color = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), heroes[i].HeroType.ToString(), true);
                if (withIndex)
                {
                    ConsoleHandler.ConsoleProvider.Write("{0}. ", color, i + 1);
                }
                if (moreInfo)
                {
                    ConsoleHandler.ConsoleProvider.WriteLine(heroes[i].DetailedToString(), color);
                }
                else
                {
                    ConsoleHandler.ConsoleProvider.WriteLine(heroes[i].ToString(), color);
                }
            }
        }

        public static void PrintHeroName(Hero hero, bool newLine)
        {
            ConsoleColor color = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), hero.HeroType.ToString(), true);
            if (newLine)
            {
                ConsoleHandler.ConsoleProvider.WriteLine(hero.Name, color);
            }
            else
            {
                ConsoleHandler.ConsoleProvider.Write(hero.Name, color);
            }
        }

        public static void PrintInfo(Player player)
        {
            ConsoleHandler.ConsoleProvider.WriteLine(Const.Info, player.DiamondPieces);
            PrintHeroes(player.Heroes, false, true);
        }

        public static void BadStateCommand(Command command)
        {
            ConsoleHandler.ConsoleProvider.WriteLine(Const.BadStateCommand, command.FullCommand);
        }

        public static void BadCommand(Command command)
        {
            ConsoleHandler.ConsoleProvider.WriteLine(Const.BadCommand, command.FullCommand);
        }

        public static void PrintRound(int round, Hero attacker, Hero protector)
        {
            ConsoleHandler.ConsoleProvider.Write(Const.Round, round);
            PrintHeroName(attacker, false);
            ConsoleHandler.ConsoleProvider.Write(" vs. ");
            PrintHeroName(protector, true);
        }

        public static void PrintDefeated(Hero hero)
        {
            PrintHeroName(hero, false);
            ConsoleHandler.ConsoleProvider.WriteLine(Const.Defeated);
        }

        public static void PrintCurrentHealth(Hero hero)
        {
            PrintHeroName(hero, false);
            ConsoleHandler.ConsoleProvider.WriteLine(Const.CurrentHealth, hero.Health);
        }

        public static void PrintDamageDealt(int damage, Hero attacker, Hero defender)
        {
            PrintHeroName(attacker, false);
            ConsoleHandler.ConsoleProvider.Write(Const.DamageDealt, damage);
            PrintHeroName(defender, true);
        }

        public static void CaveEnd(Player player, bool win)
        {
            if (win)
            {
                ConsoleHandler.ConsoleProvider.WriteLine(Const.CavePassed, 150);
                player.LevelUp(150);
                player.DiamondPieces++;
            }
            else
            {
                ConsoleHandler.ConsoleProvider.WriteLine(Const.CaveFailed, 100);
                player.LevelUp(100);
            }
        }

        public static void Finish()
        {
            ConsoleHandler.ConsoleProvider.Write(Const.Finish);
            ConsoleHandler.ConsoleProvider.WaitForRandomKey();
        }
    }
}
