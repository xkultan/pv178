﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hw01
{
    public class Player
    {
        public List<Hero> Heroes { get; set; }
        public int DiamondPieces { get; set; }
        public int Fight { get; set; }

        public Player()
        {
            Heroes = new List<Hero>();
            DiamondPieces = 0;
        }

        public void LevelUp(int xp)
        {
            if (Heroes[0].Xp + xp >= Heroes[0].MaxXp)
            {
                ConsoleHandler.ConsoleProvider.WriteLine(Const.LevelUp);
            }
            Heroes.ForEach(hero => hero.LevelUp(xp));
        }
    }
}
