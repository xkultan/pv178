﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hw01
{
    class ConsoleProvider : IConsoleProvider
    {
        public void Write(string text, params object[] strings)
        {
            Console.Write(text, strings);
        }

        public void Write(string text, ConsoleColor color, params object[] strings)
        {
            Console.ForegroundColor = color;
            Write(text, strings);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void WriteLine(string text, params object[] strings)
        {
            Console.WriteLine(text, strings);
        }

        public void WriteLine(string text, ConsoleColor color, params object[] strings)
        {
            Console.ForegroundColor = color;
            WriteLine(text, strings);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public string Read()
        {
            Write(Const.Prompt);
            return Console.ReadLine();
        }

        public void WaitForRandomKey()
        {
            Console.ReadKey();
            return;
        }
    }
}
