﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hw01
{
    public interface IConsoleProvider
    {
        public void Write(string text, params object[] strings);

        public void Write(string text, ConsoleColor color, params object[] strings);

        public void WriteLine(string text, params object[] strings);

        public void WriteLine(string text, ConsoleColor color, params object[] strings);

        public string Read();

        public void WaitForRandomKey();
    }
}
