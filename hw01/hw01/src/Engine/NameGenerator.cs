﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hw01
{
    public class NameGenerator
    {
        private static Random random = new Random();
        private List<string> GeneratedNames = new List<string>();

        public string GenerateName()
        {
            string name;
            do
            {
                if (GeneratedNames.Count == Const.HeroName.Count)
                {
                    Reset();
                }
                name = Const.HeroName[random.Next(Const.HeroName.Count)];
            } while (GeneratedNames.Contains(name));
            GeneratedNames.Add(name);
            return name;
        }

        public void Reset()
        {
            GeneratedNames.Clear();
        }
    }

}
