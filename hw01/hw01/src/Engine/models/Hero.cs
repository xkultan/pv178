﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hw01
{
    public class Hero : Entity
    {

        public HeroTypeEnum HeroType { get; }

        public Hero(string name, int health, int damage,
            int speed, int level, HeroTypeEnum heroType) : base(name, health, damage, speed, level)
        {
            HeroType = heroType;
        }

        public override void Attack(Entity another)
        {
            AttackHero((Hero)another);
        }

        public void AttackHero(Hero other)
        {
            if (Speed < other.Speed)
            {
                other.Defend(this);
                if (Health <= 0)
                {
                    return;
                }
            }
            if (Const.EficiencyAttack[HeroType].Contains(other.HeroType))
            {
                Utils.PrintDamageDealt(Damage * 2, this, other);
                other.Health -= Damage * 2;
            }
            else
            {
                Utils.PrintDamageDealt(Damage, this, other);
                other.Health -= Damage;
            }
            if (other.Health <= 0)
            {
                Utils.PrintDefeated(other);
                return;
            }
            if (Speed >= other.Speed)
            {
                other.Defend(this);
            }
        }

        public void Defend(Hero other)
        {
            if (Const.EficiencyAttack[HeroType].Contains(other.HeroType))
            {
                Utils.PrintDamageDealt(Damage * 2, this, other);
                other.Health -= Damage * 2;
            }
            else
            {
                Utils.PrintDamageDealt(Damage, this, other);
                other.Health -= Damage;
            }
            if (other.Health <= 0)
            {
                Utils.PrintDefeated(other);
            }
        }

        public override string ToString()
        {
            return Name + ": " + Damage + " Attack, " + Health + " HP, " + Speed + " Speed";
        }

        public string DetailedToString()
        {
            return ToString() + ", level " + Level + ", " + Xp + "/" + MaxXp + " XP";
        }

        public void LevelUp(int xp)
        {
            Xp += xp;
            Health = MaxHealth;
            while (Xp > MaxXp)
            {
                Level++;
                Xp -= MaxXp;
                MaxHealth = Health = (int)(MaxHealth * 1.2);
                Damage = (int)(Damage * 1.2);
                Speed = (int)(Speed * 1.2);
            }
        }
    }
}
