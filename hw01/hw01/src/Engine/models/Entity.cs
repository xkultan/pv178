﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hw01
{

    public abstract class Entity
    {
        public string Name { get; }
        public int Health { get; set; }
        public int MaxHealth { get; set; }
        public int Damage { get; set; }
        public int Speed { get; set; }
        public int Level { get; set; }
        public int MaxXp { get; }
        public int Xp { get; set; }

        protected Entity(string name, int health, int damage,
            int speed, int level)
        {
            Name = name;
            Health = health;
            MaxHealth = health;
            Damage = damage;
            Speed = speed;
            Level = level;
            MaxXp = 100;
            Xp = 0;
        }

        public abstract void Attack(Entity another);

        public override abstract string ToString();
    }
}
