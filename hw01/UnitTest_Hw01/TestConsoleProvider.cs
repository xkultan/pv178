﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hw01;

namespace UnitTest_Hw01
{
    class TestConsoleProvider : IConsoleProvider
    {
        public Queue<string> CommandQueue { get; set; }
        private List<string> _output = new List<string>();
        private string _unfinishedLine = "";

        public string Read()
        {
            if (CommandQueue.Count == 0)
            {
                return "exit";
            }
            return CommandQueue.Dequeue();
        }

        public void Write(string text, params object[] strings)
        {
            _unfinishedLine += string.Format(text, strings);
        }

        public void Write(string text, ConsoleColor color, params object[] strings)
        {
            Write(text, strings);
        }

        public void WriteLine(string text, params object[] strings)
        {
            _output.Add(_unfinishedLine + string.Format(text, strings));
            _unfinishedLine = "";
        }

        public void WriteLine(string text, ConsoleColor color, params object[] strings)
        {
            WriteLine(text, strings);
        }

        public List<string> GetOutput()
        {
            if (_unfinishedLine != "")
            {
                _output.Add(_unfinishedLine);
                _unfinishedLine = "";
            }
            return _output;
        }

        public void Clear()
        {
            _output = new List<string>();
            _unfinishedLine = "";
        }

        public void WaitForRandomKey()
        {
            return;
        }
    }
}
