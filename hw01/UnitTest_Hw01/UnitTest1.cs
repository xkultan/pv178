using Microsoft.VisualStudio.TestTools.UnitTesting;
using Hw01;
using System.Text;
using System.Collections.Generic;

namespace UnitTest_Hw01
{
    [TestClass]
    public class UnitTest1
    {

        private Game game = new Game();
        private readonly TestConsoleProvider consoleProvider = new TestConsoleProvider();

        [TestInitialize]
        public void StartUp()
        {
            ConsoleHandler.ConsoleProvider = consoleProvider;
            game = new Game();
        }

        [TestCleanup]
        public void Cleanup()
        {
            consoleProvider.Clear();
            Utils.NameGenerator.Reset();
        }

        [TestMethod]
        public void InitTest()
        {
            game.Init();
            List<string> expected = new List<string>();
            expected.Add(Const.StartText);
            expected.Add(Const.Delimeter);
            for (int i = 0; i < game.State.GeneratedHeroes.Count; i++)
            {
                expected.Add((i + 1) + ". " + game.State.GeneratedHeroes[i].ToString());
            }
            expected.Add(Const.Delimeter);
            CollectionAssert.AreEqual(expected, consoleProvider.GetOutput());
        }

        [TestMethod]
        public void BadStateCommands()
        {
            game.Init();
            consoleProvider.Clear();
            Queue<string> commands = new Queue<string>();
            commands.Enqueue("inspect");
            commands.Enqueue("Inspect");
            commands.Enqueue("INFO");
            commands.Enqueue("reorder");
            commands.Enqueue("reorder 5 2 1");
            consoleProvider.CommandQueue = commands;
            game.Run();
            List<string> expected = new List<string>() { Const.InspectNotStarted, Const.InspectNotStarted, string.Format(Const.BadStateCommand, "INFO"),
                                                        string.Format(Const.BadStateCommand, "reorder"), string.Format(Const.BadStateCommand, "reorder 5 2 1"),
                                                        Const.Finish};
            CollectionAssert.AreEqual(expected, consoleProvider.GetOutput());
        }

        [TestMethod]
        public void Commands()
        {
            game.Init();
            consoleProvider.Clear();
            Queue<string> commands = new Queue<string>();
            commands.Enqueue("start 2 3 5");
            commands.Enqueue("inspect");
            commands.Enqueue("info");
            commands.Enqueue("Rip");
            consoleProvider.CommandQueue = commands;
            game.Run();
            List<Hero> heroes = game.State.Player.Heroes;
            List<Hero> cave = game.State.Caves[game.State.Caves.Count - 1];
            List<string> expected = new List<string>() { string.Format("You have choosen {0}, {1} and {2}", heroes[0].Name, heroes[1].Name, heroes[2].Name), Const.Commands, Const.Inspect,
                                                        cave[0].ToString(), cave[1].ToString(), cave[2].ToString(), string.Format(Const.Info, game.State.Player.DiamondPieces),
                                                        heroes[0].DetailedToString(), heroes[1].DetailedToString(), heroes[2].DetailedToString(), Const.Rip, Const.Finish};
            CollectionAssert.AreEqual(expected, consoleProvider.GetOutput());
        }

        [TestMethod]
        public void Fight()
        {
            Hero hero = new Hero("Hero", 5, 2, 4, 0, HeroTypeEnum.red);
            Hero secondHero = new Hero("Hero2", 6, 2, 5, 0, HeroTypeEnum.green);
            hero.Attack(secondHero);
            Assert.IsTrue(hero.Health == 3 && hero.Damage == 2 && hero.Level == 0 && hero.Xp == 0);
            Assert.IsTrue(secondHero.Health == 2 && secondHero.Damage == 2 && secondHero.Level == 0 && hero.Xp == 0);
        }
    }
}