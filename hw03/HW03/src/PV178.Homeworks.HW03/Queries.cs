﻿using PV178.Homeworks.HW03.DataLoading.DataContext;
using PV178.Homeworks.HW03.DataLoading.Factory;
using PV178.Homeworks.HW03.Model;
using PV178.Homeworks.HW03.Model.Enums;
using System.Text.RegularExpressions;

namespace PV178.Homeworks.HW03
{
    public class Queries
    {
        private IDataContext? _dataContext;
        public IDataContext DataContext => _dataContext ??= new DataContextFactory().CreateDataContext();

        /// <summary>
        /// SFTW si vyžiadala počet útokov, ktoré sa udiali v krajinách začinajúcich na písmeno <'A', 'G'>,
        /// a kde obete boli muži v rozmedzí <15, 40> rokov.
        /// </summary>
        /// <returns>The query result</returns>
        public int AttacksAtoGCountriesMaleBetweenFifteenAndFortyQuery()
        {
            var countries = DataContext.Countries.Where(x => Regex.IsMatch(x.Name, "^[A-G]"));
            var people = DataContext.AttackedPeople.Where(x => x.Age >= 15 && x.Age <= 40 && x.Sex == Sex.Male);
            return DataContext.SharkAttacks.Join(countries,
                                                 attack => attack.CountryId,
                                                 country => country.Id,
                                                 (attack, country) => attack.AttackedPersonId)
                                                .Join(people,
                                                      personId => personId,
                                                      person => person.Id,
                                                      (personId, person) => personId).Count();
        }


        /// <summary>
        /// Vráti zoznam, v ktorom je textová informácia o každom človeku,
        /// ktorého meno nie je známe (začína na malé písmeno alebo číslo) a na ktorého zaútočil žralok v štáte Bahamas.
        /// Táto informácia je v tvare:
        /// {meno človeka} was attacked in Bahamas by {latinský názov žraloka}
        /// </summary>
        /// <returns>The query result</returns>

        public List<string> InfoAboutPeopleWithUnknownNamesAndWasInBahamasQuery()
        {
            return DataContext.SharkAttacks.Where(x => x.CountryId == DataContext.Countries.First(y => y.Name == "Bahamas").Id).Join(
                        DataContext.AttackedPeople.Where(x => Regex.IsMatch(x.Name, "^[a-z0-9]")),
                        attack => attack.AttackedPersonId,
                        person => person.Id,
                        (attack, person) => new { PersonName = person.Name, attack.SharkSpeciesId }).Join(DataContext.SharkSpecies,
                                                                                           attack => attack.SharkSpeciesId,
                                                                                           shark => shark.Id,
                                                                                           (attack, shark) => new { attack.PersonName, shark.LatinName }).Select(x => string.Format("{0} was attacked in Bahamas by {1}", x.PersonName, x.LatinName)).ToList();

        }

        /// <summary>
        /// Prišla nám ďalšia požiadavka od našej milovanej SFTW. 
        /// Chcú od nás 5 názvov krajín s najviac útokmi, kde žraloky merali viac ako 3 metre.
        /// Požadujú, aby tieto data boli zoradené abecedne.
        /// </summary>
        /// <returns>The query result</returns>
        public List<string> FiveCountriesWithTopNumberOfAttackSharksLongerThanThreeMetersQuery()
        {
            return DataContext.Countries.GroupJoin(
                        DataContext.SharkAttacks.Join(DataContext.SharkSpecies.Where(x => x.Length > 3),
                                                      attack => attack.SharkSpeciesId,
                                                      shark => shark.Id,
                                                      (attack, shark) => attack.CountryId),
                        country => country.Id,
                        countryId => countryId,
                        (country, countryId) => new { country.Name, countryId }
                        ).OrderBy(x => x.countryId.Count()).TakeLast(5).Select(x => x.Name).OrderBy(x => x).ToList();
        }

        /// <summary>
        /// SFTW chce zistiť, či žraloky berú ohľad na pohlavie obete. 
        /// Vráti informáciu či každý druh žraloka, ktorý je dlhší ako 2 metre
        /// útočil aj na muža aj na ženu.
        /// </summary>
        /// <returns>The query result</returns>
        public bool AreAllLongSharksGenderIgnoringQuery()
        {
            return DataContext.SharkAttacks.Join(DataContext.SharkSpecies.Where(x => x.Length > 2),
                                          attack => attack.SharkSpeciesId,
                                          shark => shark.Id,
                                          (attack, shark) => new { SharkId = shark.Id, attack.AttackedPersonId }
                                          ).GroupJoin(DataContext.AttackedPeople.Where(x => x.Sex == Sex.Male),
                                                      attack => attack.AttackedPersonId,
                                                      person => person.Id,
                                                      (attack, person) => new { attack.SharkId, attack.AttackedPersonId, Males = person }
                                                      ).GroupJoin(DataContext.AttackedPeople.Where(x => x.Sex == Sex.Female),
                                                                  attack => attack.AttackedPersonId,
                                                                  person => person.Id,
                                                                  (attack, person) => new { attack.SharkId, attack.Males, Females = person }).All(x => x.Females.Any() && x.Males.Any());
        }

        /// <summary>
        /// Každý túži po prezývke a žralok nie je výnimkou. Keď na Vás pekne volajú, hneď Vám lepšie chutí. 
        /// Potrebujeme získať všetkých žralokov, ktorí nemajú prezývku(AlsoKnownAs) a k týmto žralokom krajinu v ktorej najviac útočili.
        /// Samozrejme to SFTW chce v podobe Dictionary, kde key bude názov žraloka a value názov krajiny.
        /// Len si predstavte tie rôznorodé prezývky, napr. Devil of Kyrgyzstan.
        /// </summary>
        /// <returns>The query result</returns>
        public Dictionary<string, string> SharksWithoutNickNameAndCountryWithMostAttacksQuery()
        {
            return DataContext.SharkAttacks.Join(DataContext.Countries,
                                          attack => attack.CountryId,
                                          country => country.Id,
                                          (attack, country) => new { attack.SharkSpeciesId, CountryName = country.Name }
                                          ).Join(DataContext.SharkSpecies.Where(x => !x.AlsoKnownAs.Any()),
                                                 attack => attack.SharkSpeciesId,
                                                 shark => shark.Id,
                                                 (attack, shark) => new { attack.CountryName, SharkName = shark.Name }
                                                 ).GroupBy(x => new { x.SharkName })
                                                    .Select(x => x.GroupBy(y => new { y.CountryName }))
                                                        .Select(x => x.OrderBy(y => y.Count()))
                                                               .ToDictionary(x => x.Last().First().SharkName, x => x.Last().Key.CountryName);
        }

        /// <summary>
        /// Ohúrili ste SFTW natoľko, že si u Vás objednali rovno textové výpisy. Samozrejme, že sa to dá zvladnúť pomocou LINQ. 
        /// Chcú aby ste pre všetky fatálne útoky v štátoch na písmenko 'D' a 'E', urobili výpis v podobe: 
        /// "{Meno obete} (iba ak sa začína na veľké písmeno) was attacked in {názov štátu} by {latinský názov žraloka}"
        /// Získané pole zoraďte abecedne a vraťte prvých 5 viet.
        /// </summary>
        /// <returns>The query result</returns>
        public List<string> InfoAboutPeopleAndCountriesOnDorEAndFatalAttacksQuery()
        {
            return DataContext.SharkAttacks.Where(x => x.AttackSeverenity == AttackSeverenity.Fatal).Join(DataContext.Countries.Where(x => Regex.IsMatch(x.Name, "^[DE]")),
                                                                                 attack => attack.CountryId,
                                                                                 country => country.Id,
                                                                                 (attack, country) => new { attack.AttackedPersonId, attack.SharkSpeciesId, CountryName = country.Name })
                                                                                    .Join(DataContext.AttackedPeople.Where(x => Regex.IsMatch(x.Name, "^[A-Z]")),
                                                                                          attack => attack.AttackedPersonId,
                                                                                          person => person.Id,
                                                                                          (attack, person) => new { PersonName = person.Name, attack.CountryName, attack.SharkSpeciesId })
                                                                                              .Join(DataContext.SharkSpecies,
                                                                                                 attack => attack.SharkSpeciesId,
                                                                                                 shark => shark.Id,
                                                                                                 (attack, shark) => new { attack.PersonName, attack.CountryName, shark.LatinName })
                                                                                                  .Select(x => string.Format("{0} was attacked in {1} by {2}", x.PersonName, x.CountryName, x.LatinName))
                                                                                                      .OrderBy(x => x)
                                                                                                          .Take(5)
                                                                                                              .ToList();
        }

        /// <summary>
        /// SFTW pretlačil nový zákon. Chce pokutovať štáty v Afrike.
        /// Každý z týchto štátov dostane pokutu za každý útok na ich území a to buď 250 meny danej krajiny alebo 300 meny danej krajiny (ak bol fatálny).
        /// Ak útok nebol preukázany ako fatal alebo non-fatal, štát za takýto útok nie je pokutovaný. Vyberte prvých 5 štátov s najvyššou pokutou.
        /// Vety budú zoradené zostupne podľa výšky pokuty.
        /// Opäť od Vás požadujú neštandardné formátovanie: "{Názov krajiny}: {Pokuta} {Mena danej krajiny}"
        /// Egypt: 10150 EGP
        /// Senegal: 2950 XOF
        /// Kenya: 2800 KES
        /// </summary>
        /// <returns>The query result</returns>
        public List<string> InfoAboutFinesOfAfricanCountriesTopFiveQuery()
        {
            return DataContext.Countries.Where(x => x.Continent == "Africa")
                                             .GroupJoin(DataContext.SharkAttacks,
                                                        country => country.Id,
                                                        attack => attack.CountryId,
                                                        (country, attack) => new { CountryName = country.Name, country.CurrencyCode, attack })
                                                            .Select(x => new { x.CountryName, x.CurrencyCode, Penalty = x.attack.Select(y => y.AttackSeverenity == AttackSeverenity.Fatal ? 300 : 250).Sum() })
                                                                .OrderByDescending(x => x.Penalty)
                                                                    .Take(5)
                                                                        .Select(x => string.Format("{0}: {1} {2}", x.CountryName, x.Penalty, x.CurrencyCode))
                                                                            .ToList();
        }

        /// <summary>
        /// CEO chce kandidovať na prezidenta celej planéty. Chce zistiť ako ma štylizovať svoju rétoriku aby zaujal čo najviac krajín.
        /// Preto od Vás chce, aby ste mu pomohli zistiť aké percentuálne zastúpenie majú jednotlivé typy vlád.
        /// Požaduje to ako jeden string: "{typ vlády}: {percentuálne zastúpenie}%, ...". 
        /// Výstup je potrebné mať zoradený, od najväčších percent po najmenšie a percentá sa budú zaokrúhľovať na jedno desatinné číslo.
        /// Pre zlúčenie použite Aggregate(..).
        /// </summary>
        /// <returns>The query result</returns>
        public string GovernmentTypePercentagesQuery()
        {
            var a = Enum.GetValues(typeof(GovernmentForm)).Cast<GovernmentForm>()
                    .GroupJoin(DataContext.Countries,
                               govermentForm => govermentForm,
                               country => country.GovernmentForm,
                               (governmentForm, countries) => new Tuple<GovernmentForm, int>(governmentForm, countries.Count()));
            double count = a.Sum(x => x.Item2);
            return a.Select(x => new { GovernmentForm = x.Item1, Percentage = (x.Item2 / count) * 100 })
                        .OrderByDescending(x => x.Percentage)
                            .Where(x => x.Percentage > 0)
                                .Select(x => string.Format("{0}: {1}%", x.GovernmentForm, x.Percentage.ToString("F1")))
                                    .Aggregate((x, y) => x + ", " + y);
        }

        /// <summary>
        /// Oslovili nás surfisti. Chcú vedieť, či sú ako skupina viacej ohrození žralokmi. 
        /// Súrne potrebujeme vedieť koľko bolo fatálnych útokov na surfistov("surf", "Surf", "SURF") 
        /// a aký bol ich premierný vek(zaokrúliť na 2 desatinné miesta). 
        /// Zadávateľ úlohy nám to, ale skomplikoval. Tieto údaje chce pre každý kontinent.
        /// </summary>
        /// <returns>The query result</returns>
        public Dictionary<string, Tuple<int, double>> InfoForSurfersByContinentQuery()
        {
            var pepopleAgeWIthCountryId = DataContext.AttackedPeople.Join(DataContext.SharkAttacks.Where(x => x.AttackSeverenity == AttackSeverenity.Fatal && Regex.IsMatch(x.Activity, "(surf)|(Surf)|(SURF)")),
                                                                          person => person.Id,
                                                                          attack => attack.AttackedPersonId,
                                                                          (person, attack) => new { person.Age, attack.CountryId });
            return DataContext.Countries.GroupJoin(pepopleAgeWIthCountryId,
                                            country => country.Id,
                                            person => person.CountryId,
                                            (country, person) => new { country.Continent, Count = person.Count(), AverageAge = person.Select(x => x.Age).Average() })
                                    .OrderBy(x => x.Continent).GroupBy(x => x.Continent)
                                        .Select(x => Tuple.Create(x.Key, Tuple.Create(x.Sum(y => y.Count), Math.Round(x.Average(x => x.AverageAge).GetValueOrDefault(0), 2))))
                                            .Where(x => x.Item2.Item1 != 0)
                                                .ToDictionary(x => x.Item1, x => x.Item2);
        }

        /// <summary>
        /// Zaujíma nás 10 najťažších žralokov na planéte a krajiny Severnej Ameriky. 
        /// CEO požaduje zoznam dvojíc, kde pre každý štát z danej množiny bude uvedený zoznam žralokov z danej množiny, ktorí v tom štáte útočili.
        /// Pokiaľ v nejakom štáte neútočil žiaden z najťažších žralokov, zoznam žralokov bude prázdny.
        /// SFTW požaduje prvých 5 položiek zoznamu dvojíc, zoradeného abecedne podľa mien štátov.

        /// </summary>
        /// <returns>The query result</returns>
        public List<Tuple<string, List<SharkSpecies>>> HeaviestSharksInNorthAmericaQuery()
        {
            var sharks = DataContext.SharkSpecies.OrderByDescending(x => x.Weight).Take(10);
            var countries = DataContext.Countries.Where(x => x.Continent == "North America").OrderBy(x => x.Name).Take(5);
            var attacksFromTenHeaviest = DataContext.SharkAttacks.Join(sharks,
                                                                       attack => attack.SharkSpeciesId,
                                                                       shark => shark.Id,
                                                                       (attack, shark) => new {attack.CountryId, shark});
            return countries.GroupJoin(attacksFromTenHeaviest,
                                       country => country.Id,
                                       attack => attack.CountryId,
                                       (country, attacks) => Tuple.Create(country.Name, attacks.Select(x => x.shark).Distinct().ToList())).ToList();
        }

        /// <summary>
        /// Zistite nám prosím všetky útoky spôsobené pri člnkovaní (attack type "Boating"), ktoré mal na vine žralok s prezývkou "White death". 
        /// Zaujímajú nás útoky z obdobia po 3.3.1960 (vrátane) a ľudia, ktorých meno začína na písmeno z intervalu <U, Z>.
        /// Výstup požadujeme ako zoznam mien zoradených abecedne.
        /// </summary>
        /// <returns>The query result</returns>
        public List<string> NonFatalAttemptOfWhiteDeathOnPeopleBetweenUAndZQuery()
        {
            var IdOfWhiteDeath = DataContext.SharkSpecies.First(x => x.AlsoKnownAs == "White death").Id;
            var people = DataContext.AttackedPeople.Where(x => Regex.IsMatch(x.Name, "^[U-Z]"));
            return DataContext.SharkAttacks.Where(x => x.Type == AttackType.Boating && x.DateTime > new DateTime(1960, 3, 3) && x.SharkSpeciesId == IdOfWhiteDeath)
                                        .Join(people,
                                              attack => attack.AttackedPersonId,
                                              person => person.Id,
                                              (attack, person) => person.Name)
                                            .OrderBy(x => x).ToList();
        }

        /// <summary>
        /// Myslíme si, že rýchlejší žralok ma plnší žalúdok. 
        /// Požadujeme údaj o tom koľko percent útokov má na svedomí najrýchlejší a najpomalší žralok.
        /// Výstup požadujeme vo formáte: "{percentuálne zastúpenie najrýchlejšieho}% vs {percentuálne zastúpenie najpomalšieho}%"
        /// Perc. zastúpenie zaokrúhlite na jedno desatinné miesto.
        /// </summary>
        /// <returns>The query result</returns>
        public string FastestVsSlowestSharkQuery()
        {
            var sharksBySpeed = DataContext.SharkSpecies.OrderBy(x => x.TopSpeed);
            var slowAttack = DataContext.SharkAttacks.Where(x => x.SharkSpeciesId == sharksBySpeed.First(x => x.TopSpeed != null).Id);
            var fastAttack = DataContext.SharkAttacks.Where(x => x.SharkSpeciesId == sharksBySpeed.Last().Id);
            return String.Format("{0}% vs {1}%",
                                 ((fastAttack.Count() / (double)DataContext.SharkAttacks.Count()) * 100).ToString("f1"),
                                 ((slowAttack.Count() / (double)DataContext.SharkAttacks.Count()) * 100).ToString("f1"));
        }

        /// <summary>
        /// Prišla nám požiadavka z hora, aby sme im vrátili zoznam, 
        /// v ktorom je textová informácia o KAŽDOM človeku na ktorého zaútočil žralok v štáte Bahamas.
        /// Táto informácia je taktiež v tvare:
        /// {meno človeka} was attacked by {latinský názov žraloka}
        /// 
        /// Ale pozor váš nový nadriadený ma panický strach z operácie Join alebo GroupJoin.
        /// Nariadil vám použiť metódu Zip.
        /// Zistite teda tieto informácie bez spojenia hocijakých dvoch tabuliek a s použitím metódy Zip.
        /// </summary>
        /// <returns>The query result</returns>
        public List<string> AttackedPeopleInBahamasWithoutJoinQuery()
        {
            var bahamasId = DataContext.Countries.First(x => x.Name == "Bahamas").Id;
            var attacks = DataContext.SharkAttacks.Where(x => x.CountryId == bahamasId)
                                                    .OrderBy(x => x.SharkSpeciesId)
                                                        .GroupBy(x => x.SharkSpeciesId)
                                                            .Zip(DataContext.SharkSpecies);
            return attacks.Select(x => x.First.Select(y => string.Format("{0} was attacked by {1}", DataContext.AttackedPeople.First(z => z.Id == y.AttackedPersonId).Name, x.Second.LatinName))).SelectMany(x => x).ToList();
        }

        /// <summary>
        /// Vráti počet útokov podľa mien žralokov, ktoré sa stali v Austrálii, vo formáte {meno žraloka}: {počet útokov}
        /// </summary>
        /// <returns>The query result</returns>
        public List<string> MostThreateningSharksInAustralia()
        {
            var australianId = DataContext.Countries.First(x => x.Name == "Australia").Id;
            return DataContext.SharkSpecies.GroupJoin(DataContext.SharkAttacks.Where((x) => x.CountryId == australianId),
                                                      shark => shark.Id,
                                                      attack => attack.SharkSpeciesId,
                                                      (shark, attacks) => String.Format("{0}: {1}", shark.Name, attacks.Count())).ToList();
        }
    }
}
