﻿namespace PV178_HW02.InputOutput
{
    interface IInputOutput
    {
        public void WriteLine(string str, params string[] pars);

        public void Write(string str, params string[] pars);

        public string Read(string str);

        public void WaitForKeyStroke();

        public void Clear();
    }
}
