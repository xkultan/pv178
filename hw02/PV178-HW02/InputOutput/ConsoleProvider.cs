﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PV178_HW02.InputOutput
{
    class ConsoleProvider : IInputOutput
    {
        public string Read(string str)
        {
            return Console.ReadLine();
        }

        public void Write(string str, params string[] pars)
        {
            Console.Write(str, pars);
        }

        public void WriteLine(string str, params string[] pars)
        {
            Console.WriteLine(str, pars);
        }

        public void WaitForKeyStroke()
        {
            Console.ReadKey();
        }

        public void Clear()
        {
            Console.Clear();
        }
    }
}
