﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FormulaAPI.Entities;
using PV178_HW02.InputOutput;

namespace PV178_HW02.Commands
{
    class LogCommand : Command
    {
        public LogCommand(string commandText) : base(commandText) { }

        public override void Execute()
        {
            List<Driver> drivers = FormulaAPI.F1.GetDrivers(1000);
            List<Status> statuses = FormulaAPI.F1.GetStatuses(1000);
            var filters = CommandText.Split().ToList();
            Dictionary<int, int> resultsCount = new();
            List<Tuple<int, Status>> filteredResults = new();
            filters.RemoveAt(0);

            if (filters.Count == 1)
            {
                InputOutputProvider.InputOutput.WriteLine("Bad number of params");
                return;
            }

            for (int i = 0; i < filters.Count; i += 2)
            {
                Const.Filter.TryGetValue(filters[i], out var filter);
                if (filter == null)
                {
                    InputOutputProvider.InputOutput.WriteLine("Param {0} is not valid param for Log command", filters[0]);
                    return;
                }
                drivers = drivers.Where(x => filter.Invoke(x, filters[i + 1])).ToList();
            }

            List<Result> results;
            int offset = 0;
            do
            {
                results = FormulaAPI.F1.GetResults(1000, offset);

                foreach (Result result in results)
                {
                    if (drivers.Select(x => x.Id).Contains(result.DriverId))
                    {
                        resultsCount.TryGetValue(result.DriverId, out int value);
                        resultsCount[result.DriverId] = value + 1;

                        var resultStatus = statuses.First(x => x.Id == result.StatusId);
                        if (!resultStatus.IsIgnoredStatus())
                        {
                            filteredResults.Add(new(result.DriverId, resultStatus));
                        }
                    }
                }
                offset += 1000;
            } while (results.Count >= 1000);

            filteredResults = filteredResults.Where(x => resultsCount[x.Item1] >= 10).ToList();

            Export.CsvExporter.Export("file.csv", filteredResults);
            Modelling.ModelGenerator.RunProcessMining("file.csv");
        }
    }
}
