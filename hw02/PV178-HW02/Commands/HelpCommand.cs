﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PV178_HW02.InputOutput;

namespace PV178_HW02.Commands
{
    class HelpCommand : Command
    {
        public HelpCommand(string commandText) : base(commandText) { }

        public override void Execute()
        {
            InputOutputProvider.InputOutput.WriteLine(Const.Help);
        }
    }
}
