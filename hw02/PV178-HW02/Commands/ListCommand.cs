﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PV178_HW02.InputOutput;
using FormulaAPI.Entities;

namespace PV178_HW02.Commands
{
    internal class ListCommand : Command
    {
        public ListCommand(string commandText) : base(commandText) { }

        public override void Execute()
        {
            var prms = CommandText.Split().ToList();
            prms.RemoveAt(0);
            if (prms.Count != 2)
            {
                InputOutputProvider.InputOutput.WriteLine("Bad number of params");
                return;
            }
            int count, offset;
            try
            {
                count = int.Parse(prms[0]);
                offset = int.Parse(prms[1]);
                if (count < 0 || count > 1000 || offset < 0) 
                    throw new FormatException();
            }
            catch (FormatException)
            {
                InputOutputProvider.InputOutput.WriteLine("Bad params type, both params must be int, first beetwen 1 and 1000 and second > 0");
                return;
            }

            FormulaAPI.F1.GetDrivers(count, offset).ForEach(x => InputOutputProvider.InputOutput.WriteLine(x.StringFormat()));
        }
    }
}
