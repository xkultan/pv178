﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PV178_HW02.InputOutput;

namespace PV178_HW02.Commands
{
    class ManCommand : Command
    {
        public ManCommand(string commandText) : base(commandText) { }

        public override void Execute()
        {
            var prms = CommandText.Split().ToList();
            prms.RemoveAt(0);

            if (prms.Count != 1)
            {
                InputOutputProvider.InputOutput.WriteLine("Bad params for man");
                return;
            }

            Type t = Utils.ParseCommand(prms[0]);
            if (t == null)
            {
                Console.WriteLine("No manual entry for {0}", prms[0]);
                return;
            }
            InputOutputProvider.InputOutput.WriteLine(Const.Man, Const.CommandsMan[t].ToArray());
        }
    }
}
