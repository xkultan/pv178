﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PV178_HW02.InputOutput;

namespace PV178_HW02.Commands
{
    public class ExitCommand : Command
    {
        public ExitCommand(string commandText) : base(commandText)
        {
        }

        public override void Execute()
        {
            InputOutputProvider.InputOutput.Write("Press any key to exit this application . . .");
            InputOutputProvider.InputOutput.WaitForKeyStroke();
            Environment.Exit(0);
        }
    }
}
