﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PV178_HW02.Commands
{
    public abstract class Command
    {
        public string CommandText { get; set; }

        public Command(string commandText) => this.CommandText = commandText;

        public abstract void Execute();
    }
}
