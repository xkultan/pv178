﻿using System.Collections;
using System.Runtime.InteropServices;
using FormulaAPI.Entities;

namespace PV178_HW02.Export
{
    internal class CsvExporter
    {
        public static void Export(string fileName, List<Tuple<int, Status>> statuses) // you can update the second argument type based on your collection
        {
            string path;
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                path = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName, fileName); // update the path if necessary (Windows)
            }
            else
            {
                path = Path.Combine(Directory.GetCurrentDirectory(), fileName); // update the path if necessary (Linux, Mac)
            }

            using FileStream fs = File.Create(path);
            using var sr = new StreamWriter(fs);

            sr.WriteLine("id;activity");

            foreach (var record in statuses)
            {
                var caseID = record.Item1;   // update this based on your collection
                var activity = record.Item2.Name; // update this based on your collection
                sr.WriteLine($"{caseID};{activity}");
            }
        }
    }
}
