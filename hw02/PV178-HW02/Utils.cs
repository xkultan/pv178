﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FormulaAPI.Entities;

namespace PV178_HW02
{
    static class Utils
    {
        public static bool FilterByDriverId(this Driver driver, string driverId) => int.TryParse(driverId, out int o) && driver.Id == o;

        public static bool FilterByDriverRef(this Driver driver, string driverRef) => driver.DriverRef.Equals(driverRef, StringComparison.OrdinalIgnoreCase);

        public static bool FilterByNationality(this Driver driver, string nationality) => driver.Nationality.Equals(nationality, StringComparison.OrdinalIgnoreCase);

        public static bool FilterByDriverName(this Driver Driver, string surname) => Driver.Surname.Equals(surname, StringComparison.OrdinalIgnoreCase);

        public static bool IsIgnoredStatus(this Status status) => Const.IgnoredStatuses.Contains(status.Name) || status.Name.Contains("Lap");

        public static string StringFormat(this Driver driver)
        {
            return string.Format("ID: {0}| Driver ref: {1}| Name: {2} {3}|DOB: {4}| Nationality: {5}", driver.Id, driver.DriverRef, driver.Forename, driver.Surname,driver.Birth, driver.Nationality);
        }

        public static Type ParseCommand(string command)
        {
            return Type.GetType("PV178_HW02.Commands." + command + "Command", false, true);
        }
    }
}
