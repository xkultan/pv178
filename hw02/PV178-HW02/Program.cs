﻿using System;
using FormulaAPI;
using PV178_HW02.Commands;

namespace PV178_HW02
{
    class Program
    {
        public static void Main()
        {
            while (true)
            {
                Console.Write(Const.Prompt);
                var str = Console.ReadLine();
                str = str.Trim();
                var ss = str.Split();
                Type type = Utils.ParseCommand(ss[0]);
                if (type == null || type == typeof(Command))
                {
                    Console.WriteLine("Bad command");
                    continue;
                }
                Command c = (Command)Activator.CreateInstance(type, str);
                c.Execute();
            }
        }
    }
}