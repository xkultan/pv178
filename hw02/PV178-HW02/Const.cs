﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FormulaAPI.Entities;

namespace PV178_HW02
{
    public static class Const
    {
        public static Dictionary<string, Func<Driver, string, bool>> Filter = new()
        {
            { "-r", Utils.FilterByDriverRef },
            { "-ref", Utils.FilterByDriverRef },
            { "-n", Utils.FilterByNationality },
            { "--nationality", Utils.FilterByNationality },
            { "-s", Utils.FilterByDriverName },
            { "--surname", Utils.FilterByDriverName },
            { "-i", Utils.FilterByDriverId },
            { "--id", Utils.FilterByDriverId }
        };

        public static List<string> IgnoredStatuses = new List<string>(){ "Finished", "Disqualified", "Retired", "Withdrew", "Not classified", "Did not qualify", "Did not prequalify",
                                                                         "Safety concerns", "Driver unwell", "Excluded", "Eye injury", "Illness"};

        public static string Prompt = ">";

        public static string Help = "Commands:\n"
                                    + "help, man, list, log, exit\n"
                                    + "For more info try man command";

        public static Dictionary<Type, List<string>> CommandsMan = new()
        {
            { typeof(Commands.ExitCommand), new() { "\t\tExit - command terimate application",
                                                    "\t\texit",
                                                    "\t\tExit is a command used to terminate running application."} },
            { typeof(Commands.ManCommand), new() { "\t\tMan - command to the application reference manual",
                                                    "\t\tman [command]",
                                                    "\t\tMan is a command used to display command function and it's usage."} },
            { typeof(Commands.HelpCommand), new() { "\t\tHelp - list avilable command of application", 
                                                    "\t\thelp", 
                                                    "\t\tHelp is command used to display all avilable commands of application."} },
            { typeof(Commands.ListCommand), new() { "\t\tList - command to list drivers", 
                                                    "\t\tlist [count] [offset]", 
                                                    "\t\tThis command is used to list [count] drivers from [offset]."} },
            { typeof(Commands.LogCommand), new() { "\t\tLog - command to log drivers", 
                                                    "\t\tlog [log options]", 
                                                    "\t\tThis command is used to filter drivers with [log options] and create .csv log and process model.\n" +
                                                   "\t\tmore filter options are used like: log -n [nationality] --surname [surname] -i [id]\n" +
                                                   "OPTIONS\n" +
                                                   "\t\t-i, --id\n" +
                                                   "\t\t\tfilter drivers by id\n" +
                                                   "\t\t-n, --natioinality\n" +
                                                   "\t\t\tfilter drivers by nationality\n" +
                                                   "\t\t-r, --ref\n" +
                                                   "\t\t\tfiler drivers by ref\n" +
                                                   "\t\t-s, --surname\n" +
                                                   "\t\t\tfilter drivers by surname"} }
        };

        public static string Man = "NAME\n" +
                                "{0}\n" +
                                "SYNOPSIS\n" +
                                "{1}\n" +
                                "DESCRPITION\n" +
                                "{2}\n";
    }
}
