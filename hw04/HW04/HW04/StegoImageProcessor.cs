﻿using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;

namespace HW04
{
    public class StegoImageProcessor
    {
        private const int BitsInByte = 8;

        // Use constructor for additional configuration

        public async Task<Image<Rgba32>> LoadImageAsync(string path) => await Task.Run(() => LoadImageAsync(path));

        public async Task SaveImageAsync(Image<Rgba32> image, string path) => await Task.Run(() => SaveImageAsync(image, path));

        public Task<Image<Rgba32>> EncodePayload(Image<Rgba32> image, byte[] payload) => Task.Run(() =>
        {
            // This can be CPU-intensive, so it can run in separate task
            int bits;
            int step = payload.Length * BitsInByte * 2 < image.Width * image.Height ? 2 : 1;
            if (payload.Length * BitsInByte * 2 < image.Width * image.Height)
            {
                bits = 1;
            }
            else if ((payload.Length * BitsInByte) / 2 <= image.Width * image.Width)
            {
                bits = 2;
            }
            else
            {
                throw new ArgumentOutOfRangeException("Payload is too big to be encoded into image");
            }

            var chunks = payload.Select(x => ByteSpliting.Split(x, bits)).SelectMany(x => x);

            image.ProcessPixelRows(accessor =>
            {
                for (int i = 0; i < (((payload.Length * BitsInByte / bits) * step) / image.Width) + 2; i++)
                {
                    Span<Rgba32> row = accessor.GetRowSpan(i);
                    for (int j = 0; j < image.Width; j += step)
                    {
                        if ((i * image.Width) / step + (j / step) >= payload.Length * BitsInByte / bits)
                        {
                            break;
                        }
                        row[j].B &= Convert.ToByte(new String('1', BitsInByte - bits) + new String('0', bits), 2);
                        row[j].B ^= chunks.ElementAt((i * image.Width) / step + (j / step));
                    }
                }
            });

            return image;
        });

        public Task<byte[]> ExtractPayload(Image<Rgba32> image, int dataSize) => Task.Run(() =>
        {
            // This can be CPU-intensive, so it can run in separate task
            List<Byte> result = new();

            int bits, step;
            step = dataSize * BitsInByte * 2 < image.Width * image.Height ? 2 : 1;
            if (dataSize * BitsInByte * 2 < image.Width * image.Height)
            {
                bits = 1;
            }
            else if ((dataSize * BitsInByte) / 2 <= image.Width * image.Width)
            {
                bits = 2;
            }
            else
            {
                throw new ArgumentOutOfRangeException("Payload is too big to be encoded into image");
            }

            image.ProcessPixelRows(accessor =>
            {
                List<Byte> chunk = new();
                for (int i = 0; i < (((dataSize * BitsInByte / bits) * step) / image.Width) + 2; i++)
                {
                    Span<Rgba32> row = accessor.GetRowSpan(i);
                    for (int j = 0; j < image.Width; j += step)
                        {
                        if ((i * image.Width) / step + (j / step) >= dataSize * BitsInByte / bits)
                        {
                            break;
                        }
                        chunk.Add(Convert.ToByte(row[j].B & bits));
                        if (chunk.Count == (BitsInByte / bits))
                        {
                            result.Add(ByteSpliting.Reform(chunk, bits));
                            chunk = new();
                        }
                    }
                }
            });

            return result.ToArray();
        });
    }
}