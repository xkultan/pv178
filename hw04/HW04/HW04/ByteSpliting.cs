﻿namespace HW04
{
    public class ByteSpliting
    {
        private const int BitsInByte = 8;

        /// <summary>
        /// Splits the byte to chunks of given size.
        /// Mind the endianness! The least significant chunks are on lower index.
        /// </summary>
        /// <param name="byte">byte to split</param>
        /// <param name="size">bits in each chunk</param>
        /// <example>Split(207,2) => [3,3,0,3]</example>
        /// <returns>chunks</returns>
        public static IEnumerable<byte> Split(byte @byte, int size)
        {
            var result = new List<byte>();
            var b = Convert.ToUInt32(new String('1', size), 2);
            for (int i = 0; i < BitsInByte / size; i++)
            {
                result.Add((byte)(@byte & b));
                @byte >>= size;
            }
            return result;
        }

        /// <summary>
        /// Reforms chunks to a byte.
        /// Mind the endianness! The least significant chunks are on lower index.
        /// </summary>
        /// <param name="parts">chunks to reform</param>
        /// <param name="size">bits in each chunk</param>
        /// <example>Split([3,3,0,3],2) => 207</example>
        /// <returns>byte</returns>
        public static byte Reform(IEnumerable<byte> parts, int size)
        {
            byte result = 0b0;
            foreach (var part in parts.Reverse())
            {
                result <<= size;
                result |= part;
            }
            return result;
        }
    }
}