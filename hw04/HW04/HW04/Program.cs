﻿using HW04;
using System.Text;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;

const string inputPath = "../../../../Data/";
const string outputPath = "../../../../Output/";

var imageNames = new[]
{
    "John_Martin_-_Belshazzar's_Feast.jpg",
    "John_Martin_-_Pandemonium.jpg",
    "John_Martin_-_Sodom_and_Gomorrah.jpg",
    "John_Martin_-_The_Great_Day_of_His_Wrath.jpg",
    "John_Martin_-_The_Last_Judgement.jpg",
    "John_Martin_-_The_Plains_of_Heaven.jpg"
};

int n = 3;


Console.WriteLine("Please provide data to encrypt:");

var data = Console.ReadLine();


var stegoObject = StegoObject.LoadObject(Samples.StringSample(), (s) => Encoding.Default.GetBytes(s)); // Inputed data will be ignored and sample will be encoded
//var stegoObject = StegoObject.LoadObject(data, (s) => Encoding.Default.GetBytes(s)); // encode inputed data

Console.WriteLine("Please provide name of image saved in Data, or enter for usage of default images:");

var input = Console.ReadLine();

if (input != "")
{
    imageNames = input.Split();
}

Console.WriteLine("Please provide number of threads to be used simultaneously, or enter to use default 3:");

input = Console.ReadLine();

if (Int32.TryParse(input, out int a))
{
    n = Int32.Parse(input);
}

StegoImageProcessor stegoImageProcessor = new();
var chunks = stegoObject.GetDataChunks(imageNames.Length);

imageNames.Zip(chunks)
    .AsParallel().WithDegreeOfParallelism(n).ForAll(x =>
    {
        var image = Image.Load<Rgba32>(inputPath + x.First);
        image = stegoImageProcessor.EncodePayload(image, x.Second).Result;
        image.SaveAsPng(outputPath + Path.GetFileNameWithoutExtension(x.First) + ".png");
        image.Dispose();
    });

var decodedData = imageNames.Zip(chunks, (imageName, chunk) => (imageName, chunk.Length))
    .AsParallel().AsOrdered().WithDegreeOfParallelism(n).Select(x =>
    {
        var image = Image.Load<Rgba32>(outputPath + Path.GetFileNameWithoutExtension(x.imageName) + ".png");
        image.ProcessPixelRows(accessor =>
        {
            var a = accessor.GetRowSpan(0);
        });
        var res = stegoImageProcessor.ExtractPayload(image, x.Length).Result;
        image.Dispose();
        return res;
    }).ToList().SelectMany(x => x).ToArray();

Console.WriteLine("Encrypted and extracted data are:");
Console.WriteLine(Encoding.Default.GetString(decodedData));
Console.Write("Press any key to close this window . . .");
Console.ReadKey();