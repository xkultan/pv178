﻿using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace HW04.Tests
{
    public class StegoImageProcessorTests
    {
        [Fact]
        public async Task EncodePayload_ExtractPayload_OneImage_Match()
        {
            var input = "The quick brown fox jumps over the lazy dog";
            var inputBytes = Encoding.Default.GetBytes(input);
            var inputImage = await Image.LoadAsync<Rgba32>("Data/Coat_of_arms_of_Bruntal.jpg");

            var stegoImageProcessor = new StegoImageProcessor();

            var stegoImage = await stegoImageProcessor.EncodePayload(inputImage, inputBytes);
            var outputBytes = await stegoImageProcessor.ExtractPayload(stegoImage, inputBytes.Length);

            var output = Encoding.Default.GetString(outputBytes);

            Assert.Equal(input, output);
        }

        [Fact]
        public async Task EncodePayload_ExtractPayload_OneImage_Match_Long()
        {
            var input = @"VŠECHNO, CO OPRAVDU POTŘEBUJU ZNÁT o tom, jak žít, co dělat a jak vůbec být, jsem se naučil v mateřské školce. Moudrost mě nečekala na vrcholu hory zvané postgraduál, ale na pískovišti v nedělní škole. Tohle jsem se tam naučil:
    O všechno se rozděl.
    Hraj fér.
    Nikoho nebij.
    Vracej věci tam, kde jsi je našel.
    Uklízej po sobě.
    Neber si nic, co ti nepatří.
    Když někomu ublížíš, řekni promiň.
    Před jídlem si umyj ruce.
    Splachuj.
    Teplé koláčky a studené mléko ti udělají dobře.
    Žij vyrovnaně - trochu se uč a trochu přemýšlej a každý den trochu maluj a kresli a tancuj a hraj si a pracuj.
    Každý den odpoledne si zdřímni.
    Když vyrazíš do světa, dávej pozor na auta, chytni někoho za ruku a drž se s ostatními pohromadě.
    Nepřestávej žasnout. Vzpomeň si na semínko v plastikovém kelímku - kořínky míří dolů a rostlinka stoupá vzhůru a nikdo vlastně neví jak a proč, ale my všichni jsme takoví.
    Zlaté rybičky, křečci a bílé myšky a dokonce i to semínko v kelímku - všichni umřou. My také.
    A nikdy nezapomeň na dětské obrázkové knížky a první slovo, které ses naučil - největší slovo ze všech - DÍVEJ SE.";
            var inputBytes= Encoding.Default.GetBytes(input);
            var inputImage = await Image.LoadAsync<Rgba32>("Data/Coat_of_arms_of_Bruntal.jpg");
            var stegoImageProcessor = new StegoImageProcessor();

            var stegoImage = await stegoImageProcessor.EncodePayload(inputImage, inputBytes);
            var outputBytes = await stegoImageProcessor.ExtractPayload(stegoImage, inputBytes.Length);

            var output = Encoding.Default.GetString(outputBytes);

            Assert.Equal(input, output);
        }
    }
}
